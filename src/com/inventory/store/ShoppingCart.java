package com.inventory.store;
import java.util.ArrayList;


public class ShoppingCart {
	private int MAX_CART_SIZE = 50;
	private int size = 0;
	private ArrayList<String> items;
	
	
	public ShoppingCart() {
		this.items = new ArrayList<String>();
	}
	
	
	public ShoppingCart(ArrayList<String> items) {
		this.items = items;
	}
	
	
	public void addItem(String itemID) {
		if (size == MAX_CART_SIZE) {
			return;
		}else {
			items.add(itemID);
			size++;
		}
	}
	
	
	public void addItem(String itemID, int quantity) {
		for (int i = 0; i < quantity; i++) {
			this.addItem(itemID);
		}
	}
	
	/*
	    Adding new items to the cart .
	 */
	public void addItem(ArrayList<String> items) {
		for (String id : items) {
			this.addItem(id);
		}			
	}
	
	/*
	   Returns the list of items in the ShoppingCart
	 */
	public ArrayList<String> getItems() {
		return this.items;
	}
	/*
	   Removes ALL items from the ShoppingCart
	 */
	public void emptyCart() {
		items.clear();
	}
	
	public void removeAll(String item) {
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i) == item) {
				size--;
			}
		}
	}
	public void removeQuantity(String item, int quantity) {
		for (int i = 0; i < quantity; i++) {
			if (items.get(i) == item) {
				size --;
			}
		}
	}
	
	/*
	  	Returns the size of the ShoppingCart.
	 */
	public int getSize() {
		return size;
	}
}
