package com.inventory.store;
/*
  Membership status and tax exempt status.
 */
public class Customer {
	private boolean memberStatus;
	private boolean taxExempt;
	
	/*
	   Set memberStatus and taxExempt to (false, false) which is the default values. 
	   No parameters taken.
	 */
	public Customer () {
		this(false, false);
	}
	/*
	  If memberStatus is 'true' means the customer is a member,if it is false then is not a member.
	  Tax Exempt Status is default(false).
	  Take one parameter.
	 */
	public Customer (boolean memberStatus) {
		this(memberStatus, false);
	}
	/*
	 	If memberStatus is 'true' means the customer is a member,if it is false then is not a member.
	 	If taxExempt is 'true' means the customer is exempt from taxes if it is false then is not exempt.
	 	Take two parameters.
	 */
	public Customer (boolean memberStatus, boolean taxExempt) {
		this.memberStatus = memberStatus;
		this.taxExempt = taxExempt;
	}
	
	/*
	 	Return true or false to show if Customer has membership.
	 */
	public boolean getMemberStatus() {
		return memberStatus;
	}
	/*
	  	Return true or false to show if Customer is tax exempt or not.
	 */
	public boolean getTaxStatus() {
		return taxExempt;
	}
}
