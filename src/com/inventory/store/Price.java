package com.inventory.store;

import java.math.BigDecimal;

import com.inventory.MockDB.ItemDO;

/*
  Built for calculating values.
 */
public class Price {
	private float MemberDisc = 0.10f;
	private float TenOrMore = 0.10f;
	private float FiveOrMore = 0.05f;
	private float TaxRate = 0.045f;

	private ItemDO database;
	
	
	private BigDecimal bdRawTotal;
	private BigDecimal bdNetTotal;
	private BigDecimal bdNetDisc;
	private BigDecimal bdTaxAmount;
	private BigDecimal bdAftTaxTotal;
	
	public Price(ItemDO database){
		this.database = database;
	}
	
	
	public void calcPurchasePrice (ShoppingCart cart, Customer customer) {
		calcRawTotal(cart);
		calcDiscount(cart.getSize(), customer.getMemberStatus());
		calcTaxTotal(customer.getMemberStatus());
	}
	
	private void calcRawTotal (ShoppingCart cart) {
		float total = 0;
		for (String id : cart.getItems()) {
			if (database.Contains(id)) {
				total += database.getItem(id).getCost();
			}
			else {
				cart.removeAll(id);
			}
		}
		bdRawTotal = new BigDecimal(total).setScale(2,BigDecimal.ROUND_HALF_EVEN);
	}
	
	private void calcDiscount (int cartSize, boolean memStatus) {
		float runningTotal = bdRawTotal.floatValue();
		float netDisc = 0.0f;
		if (cartSize >= 10) {
			netDisc = bdRawTotal.floatValue() * TenOrMore;
			
		}
		if (cartSize > 5 && cartSize < 10) {
			netDisc = netDisc + bdRawTotal.floatValue() * FiveOrMore;
		}
		if (memStatus) {
			netDisc = netDisc + (bdRawTotal.floatValue() - netDisc) * MemberDisc;
		}
		
		runningTotal = bdRawTotal.floatValue() - netDisc;
		bdNetDisc = new BigDecimal(netDisc).setScale(2,BigDecimal.ROUND_HALF_EVEN);
		bdNetTotal = new BigDecimal(runningTotal).setScale(2,BigDecimal.ROUND_HALF_EVEN);
		
	}
	
	private void calcTaxTotal (boolean taxStatus) {
		if (taxStatus) {
			bdTaxAmount = new BigDecimal(0.00);
			bdAftTaxTotal = new BigDecimal(bdNetTotal.floatValue()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}
		else {
			bdTaxAmount = new BigDecimal(bdNetTotal.floatValue() * TaxRate).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			float aftTaxTotal = bdNetTotal.floatValue() + bdTaxAmount.floatValue();
			bdAftTaxTotal = new BigDecimal(aftTaxTotal).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}
	}
	
	public void printItems(ShoppingCart cart) {
		for (String i : cart.getItems()) {
			if (database.Contains(i)) {
				System.out.printf("%-12s %5.2f\n", i,database.getItem(i).getCost());
			}
		}
	}
	
	
	public BigDecimal geBDRawTotal() {
		return bdRawTotal;
	}

	public BigDecimal getBDNetTotal() {
		return bdNetTotal;
	}
	
	public BigDecimal getBDTaxAmount() {
		return bdTaxAmount;
	}
	
	public BigDecimal getBDAftTaxTotal() {
		return bdAftTaxTotal;
	}
	
	public BigDecimal getBDNetDiscount() {
		return bdNetDisc;
	}
}
