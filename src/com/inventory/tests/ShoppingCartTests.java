package com.inventory.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.inventory.MockDB.ItemDO;
import com.inventory.store.ShoppingCart;

public class ShoppingCartTests {
	ShoppingCart cart = new ShoppingCart();
	ItemDO db = new ItemDO();
	
	@Before
	public void ResetCart() {
		cart.emptyCart();
	}
	
	/*
	 	tests for no more than 50 items per order.
	 */
	@Test
	public void CartMaxTest() {
		for (int i = 49; i < 51; i++) {
			cart.addItem("iwatch");
			assertTrue(cart.getSize() <= 50);
		}
	}
	
	/*
		tests to see if adding items and emptying the cart is functional.
	 */
	@Test
	public void TestCartSize() {
		assertEquals(0, cart.getSize());
		cart.addItem("iphone", 20);
		assertEquals(20, cart.getSize());
		
		cart.removeQuantity("iphone", 15);
		assertEquals(5, cart.getSize());
	}
}
