package com.inventory.tests;
import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import com.inventory.MockDB.ItemDO;
import com.inventory.store.Customer;
import com.inventory.store.Price;
import com.inventory.store.ShoppingCart;

public class PriceTests {
	ShoppingCart cart = new ShoppingCart();
	ItemDO db = new ItemDO();
	Price r = new Price(db);
	Customer noMemNoEx = new Customer(false, false);
	Customer noMemEx = new Customer(false, true);
	Customer memNoEx = new Customer(true, false);
	Customer memEx = new Customer(true);
	ShoppingCart cart4 = new ShoppingCart();
	
	@Before
	public void ResetTests() {
		r = new Price(db);
		cart.emptyCart();			//make sure the cart is empty from beginning
		cart.addItem("iphone"); 	// 0.19
		cart.addItem("macbook");	// 0.99
		cart.addItem("keyboard"); 	// 1.49
		cart.addItem("iwatch");		// 11.99  
									//total=14.66
	}
	@Test
	public void TestCalcRawTotal() {
		//Add items to cart and see if the price calculate right.
		r.calcPurchasePrice(cart, noMemNoEx);
		assertEquals(14.66f, r.geBDRawTotal().floatValue(), 0);

	}
	
	@Test
	public void TestEmptyCart() {
		//test if empty function works correct
		cart.emptyCart();
		r.calcPurchasePrice(cart, noMemNoEx);
		assertEquals(0, r.geBDRawTotal().floatValue(), 0);
	}
	
	@Test
	public void TestCalcNonMemberDiscount() {
		ShoppingCart discCart = new ShoppingCart();
		
		//Boundary test
		
		//Test 9 items
		discCart.addItem("macbook", 9);
		r.calcPurchasePrice(discCart, noMemNoEx);
		assertEquals(8.46f,r.getBDNetTotal().floatValue(), 0);
		
		
		//Test 10 items
		discCart.addItem("macbook");
		r.calcPurchasePrice(discCart, noMemNoEx);
		assertEquals(8.91f,r.getBDNetTotal().floatValue(), 0);
		
		
		//Test 11 items
		discCart.addItem("macbook");
		r.calcPurchasePrice(discCart, noMemNoEx);
		assertEquals(9.80f,r.getBDNetTotal().floatValue(), 0);
	}
	@Test
	public void TestCalcMemberDiscount() {
		ShoppingCart discCart = new ShoppingCart();
		
		//Test 4 items
		discCart.addItem("macbook", 4);
		r.calcPurchasePrice(discCart, memNoEx);
		assertEquals(3.56f,r.getBDNetTotal().floatValue(), 0);
		
		//Test 5 items
		discCart.addItem("macbook");
		r.calcPurchasePrice(discCart, memNoEx);
		assertEquals(4.45f,r.getBDNetTotal().floatValue(), 0);
		
		//Test 6 items
		discCart.addItem("macbook"); 
		r.calcPurchasePrice(discCart, memNoEx);
		assertEquals(5.08f,r.getBDNetTotal().floatValue(), 0);	
	}
	
	@Test
	public void TestTaxTotal () {
		ShoppingCart taxCart = new ShoppingCart();
		
		taxCart.addItem("nano", 5); 
		
		//testing tax exempt
		r.calcPurchasePrice(taxCart, noMemEx);
		assertEquals(11.97f, r.getBDAftTaxTotal().floatValue(), 0);
		
		//testing no tax-exempt
		r.calcPurchasePrice(taxCart, noMemNoEx);
		assertEquals(11.97f, r.getBDAftTaxTotal().floatValue(), 0);
		
	}
	
	
}