package com.inventory.tests;

import org.junit.runner.*;
import org.junit.runner.notification.Failure;

public class RunTests {
	private Result CustomerResult;
	private Result CartResult;
	private Result PriceResult;
	public RunTests() {
		 this.CustomerResult = JUnitCore.runClasses(CustomerTests.class);
		 this.CartResult = JUnitCore.runClasses(ShoppingCartTests.class);
		 this.PriceResult = JUnitCore.runClasses(PriceTests.class);
	}
	
	public void run() {
		for (Failure failure : CustomerResult.getFailures()) {
			System.out.println(failure.toString());
		}
		for (Failure failure : CartResult.getFailures()) {
			System.out.println(failure.toString());
		}
		for (Failure failure : PriceResult.getFailures()) {
			System.out.println(failure.toString());
		}
		
		System.out.println("CustomerTests.java passed?: " + CustomerResult.wasSuccessful());
		System.out.println("ShoppingCartTests.java passed?: " + CartResult.wasSuccessful());
		System.out.println("PriceTests.java passed?: " + PriceResult.wasSuccessful());
	}
}
