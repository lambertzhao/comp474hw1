package com.inventory.MockDB;

import java.util.ArrayList;

public class ItemDO {
	private ArrayList<Item> database;
	
	public ItemDO () {
		this.database = new ArrayList<Item>();
		
		Item item0 = new Item("iphone", .19f);
		Item item1 = new Item("airpods", .49f);
		Item item2 = new Item("macbook", .99f);
		Item item3 = new Item("ipad", 22.19f);
		Item item4 = new Item("iwatch", 11.99f);
		Item item5 = new Item("nano", 2.29f);
		Item item6 = new Item("keyboard", 1.49f);
		Item item7 = new Item("mouse", 2.99f);
		
		database.add(item0);
		database.add(item1);
		database.add(item2);
		database.add(item3);
		database.add(item4);
		database.add(item5);
		database.add(item6);
		database.add(item7);
		
		
		}
	
	public Item getItem(String itemID) {
		for (int i = 0; i < database.size(); i++) {
			if (database.get(i).getID() == itemID) {
				return database.get(i);
			}
		}
		return null;
	}

	public boolean Contains(String itemID) {
		boolean contains = false;
		for (int i = 0; i < database.size(); i++) {
			if (database.get(i).getID() == itemID) {
				contains = true;
			}
		}
		return contains;
	}
}
