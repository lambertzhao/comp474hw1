package com.inventory.MockDB;

	
   	//Mock Item contains id and cost. 
	
public class Item {
	private String id;
	private float cost;
	
	public Item(String id, float cost) {
		this.id = id;
		this.cost = cost;
	}
	
	public String getID() {
		return id;
	}
	
	public float getCost() {
		return cost;
	}
	
	public String toString() {
		return "MockItem [id=" + id + ", cost=" + cost + "]";
	}
}
